# Web Scraping with Python

O algoritmo realiza uma busca de dados no site da Amazon Brasil. Seu funcionamento consiste em buscar os resultados de um determinado produto e armazená-los em arquivos .csv para análises posteriores.
Mais especificamente, o algorítmo retorna os nomes e o preços dos produtos listados na primeira página, de uma determinada busca no site da Amazon BR. 
##### EXEMPLO: 
##### busca: "Samsung Galaxy A20s"

|                   descrição               |   valor   |
|-------------------------------------------|-----------|
|Galaxy A20s, 32GB, azul                    | R$ 900.00 |
|Celular Samsung A20s, preto                | R$ 850.00 |
|Smartphone Samsung Galaxy A20s, 32GB, azul | R$ 899.00 |




#### Tecnologias utilizadas

1. BeautifulSoup
2. Selenium
3. Pandas
4. Webdriver chrome