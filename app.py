from selenium import webdriver
from BeautifulSoup import BeautifulSoup
from webdriver_manager.chrome import ChromeDriverManager
import pandas as pd

driver = webdriver.Chrome("/home/fabricio/development/python/chromedriver")
produto = "samsung galaxy a20s"
products=[] 
prices=[] 
driver.get("https://www.amazon.com.br/s?k="+produto+"&__mk_pt_BR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&ref=nb_sb_noss_2")

content = driver.page_source
soup = BeautifulSoup(content)


n = soup.findAll('div', attrs={'class':'s-expand-height s-include-content-margin s-border-bottom'})
for a in n:
	name=a.find('span', attrs={'class':'a-size-base-plus a-color-base a-text-normal'})
	products.append(name.text)
	price=a.find('span', attrs={'class':'a-offscreen'})
	print(name.text)
	if price == None :
		prices.append("sem valor") 
	else: 
		prices.append(price.text)
		print(price)
	

df = pd.DataFrame({'Produto':products, 'Valor':prices}) 
df.to_csv('produtos-list-Samsung.csv', index=True, encoding='utf-8')

